## iVoox PHP API Client

Este producto es una demo para probar el uso de la API de iVoox en PHP (https://gitlab.com/semantic-jail/php-ivoox-api). Se ha desarrollado durante la hackaton "HackPEUM" organizada por el podcast Programar es una mierda el 3 de Febrero de 2018.

- Ibon Conesa García (@ibonconesa).
- Marco Bellido (@marcoocram).


### Cómo usarlo

Clona el repositorio y accede a la carpeta

```
git clone https://gitlab.com/semantic-jail/php-ivoox-api-client.git && cd php-ivoox-api-client.git
```

Instala las dependencias

```
composer install
```

Copia el archivo de entorno y genera una clave 

```
cp .env.example .env
php artisan key:generate
```

Configura los permisos de logs, caché y sesión

```
chown -R www-data:www-data /var/www/storage /var/www/bootstrap/cache
```


Ejecuta docker-compose para levantar la imagen

```
docker-compose up
```

Accede a la interfaz web en la dirección http://localhost:8000


Si quieres lanzarlo en producción, selecciona el archivo docker-compose para ello

```
 docker-compose --file=docker-compose-production.yml up
```