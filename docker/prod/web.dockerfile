FROM nginx:1.13-alpine

ADD ./docker/prod/vhost.conf /etc/nginx/conf.d/default.conf

# copy project to /var/www/
COPY public /var/www/public
