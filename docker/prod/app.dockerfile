FROM php:7.2-fpm-alpine

# install composer
RUN apk add --no-cache \
    curl git zip unzip && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# copy project to /var/www/
COPY ./ /var/www

# composer install dependencies
WORKDIR /var/www

RUN composer install --no-dev --no-scripts && \
    composer dump-autoload && \
    php artisan optimize

COPY ./.env.example /var/www/.env

RUN php artisan key:generate && \
    php artisan optimize && \
    chown -R www-data:www-data /var/www/storage /var/www/bootstrap/cache
