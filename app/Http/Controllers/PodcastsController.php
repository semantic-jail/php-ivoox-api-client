<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SemanticJail\IvooxAPI\IvooxAPI;

class PodcastsController extends Controller
{
    private $ivooxAPI;

    public function __construct(IvooxAPI $ivooxAPI)
    {
        $this->ivooxAPI = $ivooxAPI;
    }

    public function search(Request $request)
    {
        return $this->ivooxAPI->searchPodcast($request->search, 1);
    }

    public function searchEpisodes(Request $request)
    {
        return $this->ivooxAPI->searchEpisodes($request->podcastId, 1);
    }
}
