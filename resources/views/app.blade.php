<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">

        <title>iVoox PHP API Client</title>

        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    </head>
    <body>
        <div id="app">
            <app-header></app-header>
            <keep-alive>
            <router-view></router-view>
            </keep-alive>
        </div>

        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
