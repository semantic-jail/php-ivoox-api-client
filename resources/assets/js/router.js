import Vue from 'vue';
import VueRouter from 'vue-router';

import SearchPodcasts from './pages/SearchPodcasts';
import Podcast from './pages/Podcast';
import DevTeam from './pages/DevTeam';

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'SearchPodcasts',
      component: SearchPodcasts
    },
    {
      path: '/podcast/:podcastId',
      name: 'Podcast',
      component: Podcast
    },
    {
      path: '/semantic-jail',
      name: 'DevTeam',
      component: DevTeam
    },
  ]
})
