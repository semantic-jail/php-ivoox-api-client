<?php

namespace Tests\Feature;

use SemanticJail\IvooxAPI\IvooxAPI;
use Tests\TestCase;

class SearchPodcastsTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        app()->instance(IvooxAPI::class, new IvooxAPIStub());
    }

    /** @test */
    public function user_can_search_podcasts()
    {
        $this->withoutExceptionHandling();

        $response = $this->get('/search?search=programar es una mierda');

        $response->assertSuccessful();
        $response->assertJson([
            [
                'id' => 'f1432444',
                'name' => 'Programar Es Una Mierda',
                'thumbnail' => 'https://static-1.ivooxcdn.com/canales/6/2/1/7/6221498657126_XS.jpg',
                'ivoox_url' => 'https://www.ivoox.com/podcast-programar-es-mierda_sq_f1432444_1.html'
            ]
        ]);
    }

    /** @test */
    public function guest_can_search_episodes_from_podcast()
    {
        $this->withoutExceptionHandling();

        $response = $this->get('/searchEpisodes?podcastId=f1432444');

        $response->assertSuccessful();
        $response->assertJson([
            'name' => 'Programar Es Una Mierda',
            'episodes' => [
                [
                    'name' => 'Episodio 19 - Lecciones aprendidas',
                    'description' => 'Para el episodio de hoy hemos pensado en hablar de las experiencias que nos han hecho aprender',
                    'url' => 'https://www.ivoox.com/episodio-19-lecciones-aprendidas-audios-mp3_rf_23408968_1.html',
                    'duration' => '56:56',
                    'thumbnail' => 'https://static-1.ivooxcdn.com/canales/6/2/1/7/6221498657126_XS.jpg',
                    'likes' => 6,
                    'comments' => 1
                ]
            ]
        ]);
    }
}

class IvooxAPIStub extends IvooxAPI {
    public function searchPodcast(String $search, int $page = null)
    {
        $response = [
            [
                'id' => 'f1432444',
                'name' => 'Programar Es Una Mierda',
                'thumbnail' => 'https://static-1.ivooxcdn.com/canales/6/2/1/7/6221498657126_XS.jpg',
                'ivoox_url' => 'https://www.ivoox.com/podcast-programar-es-mierda_sq_f1432444_1.html',
            ]
        ];

        return json_encode($response);
    }

    public function searchEpisodes(String $podcastId, int $page = null)
    {
        $response = [
            'id' => 'f1432444',
            'name' => 'Programar Es Una Mierda',
            'thumbnail' => 'https://static-1.ivooxcdn.com/canales/6/2/1/7/6221498657126_XS.jpg',
            'ivoox_url' => 'https://www.ivoox.com/podcast-programar-es-mierda_sq_f1432444_1.html',
            'episodes' => [
                [
                    'name' => 'Episodio 19 - Lecciones aprendidas',
                    'description' => 'Para el episodio de hoy hemos pensado en hablar de las experiencias que nos han hecho aprender',
                    'url' => 'https://www.ivoox.com/episodio-19-lecciones-aprendidas-audios-mp3_rf_23408968_1.html',
                    'duration' => '56:56',
                    'thumbnail' => 'https://static-1.ivooxcdn.com/canales/6/2/1/7/6221498657126_XS.jpg',
                    'likes' => 6,
                    'comments' => 1,
                ]
            ]
        ];

        return json_encode($response);
    }
}
